class Counter {
  int value = 0;

  void increment() => value++;

  void decrement() => value--;

  int add(int a, int b) => a+b;
}